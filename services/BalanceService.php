<?php

namespace app\services;

use app\models\Balance;
use app\repositories\BalanceRepository;
use Yii;

class BalanceService
{
    /**
     * @var BalanceRepository
     */
    protected $balanceRepository;

    /**
     * BalanceService constructor.
     *
     * @param BalanceRepository $balanceRepository
     */
    public function __construct(
        BalanceRepository $balanceRepository
    ) {
        $this->balanceRepository = $balanceRepository;
    }

    /**
     * @return Balance|\yii\db\ActiveRecord
     */
    public function getBalance()
    {
        $balance = $this->balanceRepository->getById(Yii::$app->getUser()->getId());

        if (!$balance) {
            $balance = $this->create();
        }

        return $balance;
    }

    public function updateBalance($point)
    {
        $balance = $this->getBalance();
        $balance->point += $point;

        if(!$this->balanceRepository->save($balance)) {
            throw new \Exception('Don`t update balance!');
        }

        return $balance;
    }

    public function create()
    {
        return new Balance([
            'user_id' => Yii::$app->getUser()->getId(),
            'point'   => 0,
        ]);
    }

}