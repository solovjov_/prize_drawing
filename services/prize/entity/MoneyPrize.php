<?php

namespace app\services\prize\entity;

class MoneyPrize extends AbstractPrize implements Convertible
{
    protected $template = '_money';

    public function accept()
    {
        // TODO: Gateway api
        return true;
    }

    public function convert()
    {
        $this->entity->price = $this->exchange();

        return new LoyaltyPointPrize($this->entity, $this->balanceService);
    }

    public function exchange()
    {
        return $this->getPrice() * self::MONEY_TO_LOYALTY_POINT;
    }
}