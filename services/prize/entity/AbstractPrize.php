<?php

namespace app\services\prize\entity;

use app\models\Prize;
use app\services\BalanceService;

abstract class AbstractPrize
{
    /** @var Prize */
    protected $entity;

    protected $acceptedMessage = '<h2>Поздравляем! Вы выиграли: {{title}}.</h2>';

    protected $declinedMessage = '';

    protected $template = '_default';

    /**
     * @var BalanceService
     */
    protected $balanceService;

    public function __construct(
        Prize $entity,
        BalanceService $balanceService = null
    ) {
        $this->entity = $entity;
        $this->balanceService = $balanceService;
    }

    abstract public function accept();

    public function getTemplate()
    {
        return $this->template;
    }

    public function getMessage()
    {
        return str_replace('{{title}}', $this->getTitle(), $this->acceptedMessage);
    }

    public function getId()
    {
        return $this->entity->id;
    }

    public function getTitle()
    {
        return $this->entity->title;
    }

    public function getPrice()
    {
        return $this->entity->price;
    }
}