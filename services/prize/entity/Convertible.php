<?php

namespace app\services\prize\entity;

interface Convertible
{
    const MONEY_TO_LOYALTY_POINT = 10;

    public function convert();

    public function exchange();
}