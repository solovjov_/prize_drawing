<?php

namespace app\services\prize\entity;

class LoyaltyPointPrize extends AbstractPrize
{
    public function accept()
    {
        return $this->balanceService->updateBalance($this->entity->price);
    }
}