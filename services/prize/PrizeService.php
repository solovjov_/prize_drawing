<?php

namespace app\services\prize;

use app\models\Prize;
use app\models\PrizeType;
use app\repositories\PrizeRepository;
use app\services\BalanceService;
use app\services\prize\entity\LoyaltyPointPrize;
use app\services\prize\entity\MoneyPrize;
use app\services\prize\entity\ThingPrize;

class PrizeService
{
    /**
     * @var PrizeRepository
     */
    protected $prizeRepository;

    /**
     * @var BalanceService
     */
    protected $balanceService;

    /**
     * PrizeService constructor.
     *
     * @param PrizeRepository $prizeRepository
     * @param BalanceService $balanceService
     */
    public function __construct(
        PrizeRepository $prizeRepository,
        BalanceService $balanceService
    ) {
        $this->prizeRepository = $prizeRepository;
        $this->balanceService = $balanceService;
    }

    public function getPrize()
    {
        $prize = $this->prizeRepository->getAnyPrize();

        return $this->createHandler($prize);
    }

    public function acceptPrize(Prize $prize)
    {
        if ($this->prizeRepository->saveUserPrize($prize->id)) {
            return $this->createHandler($prize);
        }

        return null;
    }

    public function createHandler(Prize $prize)
    {
        $handler = null;

        switch ($prize->type_id) {
            case PrizeType::MONEY :
                $handler = new MoneyPrize($prize, $this->balanceService);
                break;
            case PrizeType::THING :
                $handler = new ThingPrize($prize);
                break;
            default:
                $handler = new LoyaltyPointPrize($prize, $this->balanceService);
        }

        return $handler;
    }
}