<?php

namespace app\resolvers;

use yii\web\Request;

interface ResolverInterface
{
    /**
     * @param \ReflectionParameter $param
     * @return bool
     */
    public function support(\ReflectionParameter $param): bool;

    /**
     * @param Request $request
     * @param \ReflectionParameter $param
     * @return \Generator
     */
    public function resolve(Request $request, \ReflectionParameter $param);
}