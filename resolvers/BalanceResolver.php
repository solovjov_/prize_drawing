<?php

namespace app\resolvers;

use app\models\Balance;
use app\services\BalanceService;
use yii\web\Request;

class BalanceResolver implements ResolverInterface
{
    /**
     * @var BalanceService
     */
    private $balanceService;

    public function __construct(BalanceService $balanceService)
    {
        $this->balanceService = $balanceService;
    }

    /**
     * @param \ReflectionParameter $param
     * @return bool
     */
    public function support(\ReflectionParameter $param): bool
    {
        return $param->getClass()->getName() == Balance::class;
    }

    /**
     * @param Request $request
     * @param \ReflectionParameter $param
     * @return \Generator
     */
    public function resolve(Request $request, \ReflectionParameter $param)
    {
        yield $this->balanceService->getBalance();
    }
}