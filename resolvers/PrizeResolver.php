<?php

namespace app\resolvers;

use app\models\Prize;
use yii\web\Request;

class PrizeResolver implements ResolverInterface
{

    /**
     * @param Request $request
     * @param \ReflectionParameter $param
     * @return \Generator
     */
    public function resolve(Request $request, \ReflectionParameter $param)
    {
        $prize = new Prize();

        if ($request->isGet) {
            $prize = Prize::findOne($request->get($param->getName())) ?? $prize;
        } elseif ($request->isPost) {
            $prize->load($request->post());
        }

        yield $prize;
    }

    /**
     * @param \ReflectionParameter $param
     * @return bool
     */
    public function support(\ReflectionParameter $param): bool
    {
        return $param->getClass()->getName() == Prize::class;
    }
}