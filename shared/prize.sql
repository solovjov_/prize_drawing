-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `prize` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `prize`;

DROP TABLE IF EXISTS `balance`;
CREATE TABLE `balance` (
  `user_id` int(11) NOT NULL,
  `point` int(11) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `balance` (`user_id`, `point`) VALUES
(100,	0);

DROP TABLE IF EXISTS `prize`;
CREATE TABLE `prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `price` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `prize_type_index` (`type_id`),
  CONSTRAINT `prize_prize_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `prize_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `prize` (`id`, `name`, `title`, `type_id`, `price`) VALUES
(1,	'first prize',	'100$',	1,	100),
(2,	'second prize',	'1000lp',	2,	1000),
(3,	'third prize',	'Торт',	3,	0),
(4,	'4',	'200$',	1,	200),
(5,	'5',	'Автомобиль',	3,	0),
(6,	'second prize',	'2000lp',	2,	2000),
(7,	'second prize',	'1500lp',	2,	1500);

DROP TABLE IF EXISTS `prize_type`;
CREATE TABLE `prize_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lim` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `prize_type` (`id`, `name`, `lim`) VALUES
(1,	'MONEY',	2),
(2,	'LOYALTY_POINT',	NULL),
(3,	'THING',	3);

DROP TABLE IF EXISTS `user_prize`;
CREATE TABLE `user_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `prize_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_prize_prize_id_fk` (`prize_id`),
  CONSTRAINT `user_prize_prize_id_fk` FOREIGN KEY (`prize_id`) REFERENCES `prize` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- 2018-11-22 10:54:42
