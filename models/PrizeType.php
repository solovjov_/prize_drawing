<?php

namespace app\models;

/**
 * This is the model class for table "prize_type".
 *
 * @property int $id
 * @property string $name
 * @property int $lim
 * @property Prize[] $prizes
 */
class PrizeType extends \yii\db\ActiveRecord
{
    const
        MONEY = 1,
        LOYALTY = 2,
        THING = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prize_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['lim'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Name',
            'lim'  => 'Lim',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrizes()
    {
        return $this->hasMany(Prize::class, ['type_id' => 'id']);
    }
}
