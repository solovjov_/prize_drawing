<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_prize".
 *
 * @property int $user_id
 * @property int $prize_id
 * @property int $id
 *
 * @property Prize $prize
 */
class UserPrize extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_prize';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'prize_id'], 'integer'],
            [['prize_id'], 'required'],
            [['prize_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prize::class, 'targetAttribute' => ['prize_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'prize_id' => 'Prize ID',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrize()
    {
        return $this->hasOne(Prize::class, ['id' => 'prize_id']);
    }
}
