<?php

namespace app\repositories;

use app\models\Balance;
use Yii;

class BalanceRepository extends AbstractRepository
{
    protected static $entity = Balance::class;

}