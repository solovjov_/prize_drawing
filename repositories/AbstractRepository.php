<?php

namespace app\repositories;

use yii\db\ActiveRecord;

class AbstractRepository {
    /**
     * @var ActiveRecord
     */
    protected static $entity;

    /**
     * @param $id
     * @return ActiveRecord
     */
    public function getById($id)
    {
        if (!static::$entity) {
            throw new \Exception('Set static property $entity');
        }

        return static::$entity::findOne($id);
    }

    public function save(ActiveRecord $entity) {
        return $entity->save();
    }
}