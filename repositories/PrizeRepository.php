<?php

namespace app\repositories;

use app\models\Prize;
use app\models\PrizeType;
use app\models\UserPrize;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

class PrizeRepository extends AbstractRepository
{
    protected static $entity = Prize::class;

    /**
     * @return Prize|ActiveRecord
     */
    public function getAnyPrize() {
        return Prize::find()
            ->alias('p')
            ->innerJoin(PrizeType::tableName() . ' pt', 'p.type_id=pt.id')
            ->where([
                'not in',
                'p.type_id',
                UserPrize::find()
                    ->alias('up')
                    ->select('pp.type_id')
                    ->leftJoin(Prize::tableName() . ' pp', '`pp`.`id`=`up`.`prize_id`')
                    ->where(['`up`.`user_id`' => Yii::$app->getUser()->getId()])
                    ->andWhere('`pp`.`type_id`=`p`.`type_id`')
                    ->groupBy('`pp`.`type_id`')
                    ->having(new Expression('count(`pp`.`type_id`) > `pt`.`lim`')),
            ])
            ->orderBy(new Expression('rand()'))
            ->one();
    }

    public function saveUserPrize($prizeId) {
        $rec = new UserPrize();

        $rec->setAttributes([
            'user_id'  => Yii::$app->getUser()->getId(),
            'prize_id' => $prizeId,
        ]);

        return $this->save($rec);
    }

    public function getReceivedPrizes() {
        return UserPrize::find()
            ->indexBy('idx')
            ->select([
                'p.*',
                'up.id idx'
            ])
            ->alias('up')
            ->leftJoin(Prize::tableName() . ' p', '`p`.`id`=`up`.`prize_id` and `up`.`user_id`=' . Yii::$app->getUser()->getId())
            ->asArray()
            ->all();
    }

}