<?php

namespace app\controllers;

use app\models\Balance;

class ProfileController extends AbstractSecurityController
{
    public function actionIndex(Balance $balance)
    {
        return $this->render('index', ['balance' => $balance]);
    }
}
