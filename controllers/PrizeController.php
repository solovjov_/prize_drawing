<?php

namespace app\controllers;

use app\models\Balance;
use app\models\Prize;
use app\services\BalanceService;
use app\services\prize\entity\Convertible;
use app\services\prize\PrizeService;
use yii\base\Module;
use yii\helpers\Json;

class PrizeController extends AbstractSecurityController
{
    /**
     * @var PrizeService
     */
    protected $prizeService;

    /** @var BalanceService */
    protected $balanceService;

    public function __construct(
        string $id,
        Module $module,
        PrizeService $prizeService,
        BalanceService $balanceService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->prizeService = $prizeService;
        $this->balanceService = $balanceService;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetPrize()
    {
        $prize = $this->prizeService->getPrize();
        $template = 'partials/' . $prize->getTemplate();

        return $this->renderPartial($template, ['prize' => $prize]);
    }

    public function actionAcceptPrize(Prize $prize, $convert = false)
    {
        if (!$prize->id) {
            throw new \Exception('Отсутсвуют данные');
        }

        $handler = $this->prizeService->acceptPrize($prize);

        if ($convert && $handler instanceof Convertible) {
            $handler = $handler->convert();
        }

        $handler->accept();
        $balance = $this->balanceService->getBalance();

        return Json::encode(['balance' => $balance->point]);
    }
}
