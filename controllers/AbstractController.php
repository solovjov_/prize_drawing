<?php

namespace app\controllers;

use app\resolvers\ResolverInterface;
use Yii;
use yii\web\Controller;

class AbstractController extends Controller
{
    /**
     * @param string $id
     * @param array $params
     * @return mixed
     */
    public function runAction($id, $params = [])
    {
        $methodName = 'action' . str_replace(' ', '', ucwords(implode(' ', explode('-', $id))));
        $ref = new \ReflectionMethod($this, $methodName);
        $arguments = $ref->getParameters();

        foreach ($arguments as $argument) {
            if (!$argument->getClass()) {
                continue;
            }

            $resolve = null;

            foreach (Yii::$container->getDefinitions() as $definition) {
                $object = Yii::createObject($definition);

                if ($object instanceof ResolverInterface && $object->support($argument)) {
                    $resolve = $object->resolve(Yii::$app->request, $argument)->current();
                    break;
                }
            }

            if (!$resolve) {
                $resolve = Yii::createObject($argument->getClass()->getName());
            }

            $params[$argument->getName()] = $resolve;
        }

        return parent::runAction($id, $params);
    }
}