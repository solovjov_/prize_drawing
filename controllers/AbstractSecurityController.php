<?php

namespace app\controllers;

use yii\filters\AccessControl;

abstract class AbstractSecurityController extends AbstractController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }
}