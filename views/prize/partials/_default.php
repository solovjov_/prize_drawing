<?php

/** @var AbstractPrize $prize */

use app\services\prize\entity\AbstractPrize;
use yii\helpers\Html;

?>

<h4><?= $prize->getTitle() ?></h4>
<footer class="modal-footer">
    <?= Html::button('Accept', ['class' => 'btn btn-success js-accept-prize', 'data-prize' => $prize->getId()]); ?>
    <?= Html::button('Decline', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']); ?>
</footer>
