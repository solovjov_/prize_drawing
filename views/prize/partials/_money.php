<?php

/** @var AbstractPrize|Convertible $prize */

use app\services\prize\entity\AbstractPrize;
use app\services\prize\entity\Convertible;
use yii\helpers\Html;

?>

<h4><?= $prize->getTitle() ?> or <?= $prize->exchange() ?> loyalty points!</h4>
<footer class="modal-footer">
    <?= Html::button('Accept money', ['class' => 'btn btn-success js-accept-prize', 'data-prize' => $prize->getId()]); ?>
    <?= Html::button('Accept point', ['class' => 'btn btn-primary js-convert-prize', 'data-prize' => $prize->getId()]); ?>
    <?= Html::button('Decline', ['class' => 'btn btn-danger', 'data-dismiss' => 'modal']); ?>
</footer>
