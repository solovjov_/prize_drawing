<?php
/**
 * @var View $this
 * @var \app\models\Prize $prize
 * @var Balance $balance
 */

use app\models\Balance;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;

?>
    <h1><?= Html::encode('Получить Приз!') ?></h1>
    <div id="balance" class="pull-right">Баланс: <span><?= $balance->point ?></span></div>
    <div id="received-prize"></div>
<?php Modal::begin([
    'header'       => '<h2>Сongratulate!</h2>',
    'id'           => 'modal-prize',
    'options'      => [
        'data-target' => '#target-prize',
    ],
    'toggleButton' => [
        'label' => 'Get Prize',
        'class' => 'btn btn-primary js-prize-drawing',

    ],
]); ?>

    <div id="target-prize">
        <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
    </div>

<?php
Modal::end();

$css = <<<CSS
#balance{
font-size: 28px;
}
#balance > span{
color:darkred;
}
CSS;

$this->registerCss($css);

$js = <<<JS
    (function($) {
        $(document).ready(function() {
            var balance = $('#balance > span');
            $(document)
                .on('hidden.bs.modal', function(e) {
                    var target = $($(e.target).data('target'));                        
                    target.html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
                })
                .on('shown.bs.modal', function(e) {
                    var target = $($(e.target).data('target'));                        
                    
                    $.get('/prize/get-prize', function(data) {
                        target.html(data);
                    })
                })
                .on('click', '.js-accept-prize', function() {
                    var modal = $('#modal-prize');
                    
                    $.get('/prize/accept-prize', $(this).data(), function(data) {
                        modal.modal('hide');
                        data = JSON.parse(data);
                        balance.html(data.balance);
                    })
                })
                .on('click', '.js-convert-prize', function() {
                    var 
                        modal = $('#modal-prize'),
                        prizeId = $(this).data('prize');
                    
                    $.get('/prize/accept-prize', {prize:prizeId, convert:true}, function(data) {
                        modal.modal('hide');
                        data = JSON.parse(data);
                        balance.html(data.balance);
                    })
                })
        });
    })(jQuery);
JS;

$this->registerJs($js, View::POS_READY);